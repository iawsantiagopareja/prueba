import "bootstrap/dist/css/bootstrap.min.css"
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import {createAuth0} from '@auth0/auth0-vue'




createApp(App)
    .use(router)
    .use(
        createAuth0({
          domain: 'https://santydeveloper.us.auth0.com',
          client_id: 'w2hbit9xA4D33tZZfpEq9Tb3ASYlRn5I',
          redirect_uri: window.location.origin,
          useRefreshTokens: true,
          cacheLocation: 'localstorage',
          audience: 'http://vue-project-santy.vercel.app/api',
        }))
    .mount('#app')


import "bootstrap/dist/js/bootstrap"
